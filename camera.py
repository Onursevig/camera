import piggyphoto
from datetime import datetime
from datetime import timedelta
import socket
import time

UDP_IP = "192.168.1.133"
UDP_PORT = 15000

C = piggyphoto.camera()
#print C.abilities

for i in range(1,10):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind((UDP_IP, UDP_PORT))
    data, addr = sock.recvfrom(1024)
    sock.close()
    #print("received info :", data)
    str1 = data.decode()
    newshottime = str1.split('_')[4]
    newshottime = newshottime.replace(':','.',2)
    print(newshottime)
    C.capture_image("/home/odroid/ftp/files/"+str(newshottime)+".jpg")
